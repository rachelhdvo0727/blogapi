from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import Post


# Transform data into JSON
class PostSerializer(serializers.ModelSerializer):
    # Specify which fields to include/exclude
    class Meta:
        fields = ('id', 'author', 'title', 'body', 'created_at',)
        model = Post


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'username',)
