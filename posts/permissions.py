from rest_framework import permissions


class IsAuthorOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        """
        return 'True' if permission is granted, 'false otherwise.
        """
        # Allow read-only permissions for any requests
        if request.method in permissions.SAFE_METHODS:
            return True

        # Allow write permissions for a post's author
        return obj.author == request.user
